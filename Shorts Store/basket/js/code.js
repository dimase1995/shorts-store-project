import { FETCH } from "../../code/request.js";

let getId = JSON.parse(localStorage.idBD);
let filt = [];

function showCards(data) {
  filt = data.filter((element) => getId.find((id) => id === element.id));
  creatCartProducts(filt);
}

FETCH("https://store-demo-be.onrender.com/api/products", showCards);
//

function creatCartProducts(products) {
  const tbody = document.querySelector("#cart-table-products");

  if (tbody.querySelector("div")) {
    tbody.querySelector("div").remove();
  }
  const cardContainer = document.createElement("div");
  cardContainer.classList.add("cart-product");
  tbody.append(cardContainer);
  products.forEach((el) => {
    console.log(el);
    const info = `<div class="cart-about-product">
  <div class="cart-product-img"><img src="${el.availableOptions[0].optionImages[0]}" alt="${el.productName}"></div>
  <div class="cart-product-description">
      <div class="cart-product-name">${el.productName}</div>
      <div class="cart-product-color-container">
          <span>Color:</span><div class="cart-product-color" style ="background-color:#${el.optionColorCode}"></div>
      </div>
      <div class="cart-product-size-container">
          <span>Size:</span><div class="cart-product-size">${el.availableOptions[0].prices[0].size}</div>
      </div>
  </div>
  <div class="cart-product=price">${el.availableOptions[0].prices[0].price} UAH</div>
  <div class="card-products">Кількість - 1</div>
  <div class="delete" data-set="${el.id}">&#x2715</div>
</div>
<div class="part2">

</div>
  `;
    cardContainer.insertAdjacentHTML("beforeend", info);
  });
}
//delete
document
  .querySelector("#cart-table-products")
  .addEventListener("click", (e) => {
    console.log(e);
    if (e.target.classList.contains("delete")) {
      deleteProduct(e.target.dataset.set);
    }
  });
function deleteProduct(productId) {
  let newData = filt.filter((el) => el.id !== productId);
  localStorage.idBD = JSON.stringify(newData);
  console.log(newData);
  showCards(newData);
}
document.querySelector(".btn").addEventListener("click", () => {
  alert("Покупка успішна");
});
