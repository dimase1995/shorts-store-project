import { AJAX, FETCH } from "./request.js";
import { randomProduct } from "./method.js";
const url = "https://store-demo-be.onrender.com/api/products";
//https://store-demo-be.onrender.com/api/products

AJAX(url, "GET", show);
//show отримує дані із сервера, якщо це масив записує в dataMain
let dataMain = [];
function show(data) {
  if (Array.isArray(data)) {
    dataMain = data;
    //виклик fn
    showRandomProducts(randomProduct(dataMain, 4));
    //console.log(randomProduct(dataMain, 5));
    console.log(data);
  } else {
    throw new Error("NOT ARRAY - ERROR!");
  }
}

const dataList = document.querySelector("#product-name");
// пошук інпут на головній сторінці //
//Звернення до інпут
const searchInput = document.querySelector(".search > input");
searchInput.addEventListener("input", (event) => {
  // очищення перед новим пошуком
  dataList.innerHTML = "";
  //filter товару + productName
  const foundedItems = dataMain.filter((el) => {
    return `${el.manufacturerName} ${el.productName.toLowerCase()}`.includes(
      event.target.value
    );
    // console.log(el.productName);
  });
  //   console.log(foundedItems);
  //створення списка випадаючого пошуку - щоб підтягувало пошук
  foundedItems.forEach((elem) => {
    //створення нового тега
    const option = document.createElement("option");
    //Запис
    option.value = elem.productName;
    //dataList+
    dataList.append(option);
  });
  //   console.log(event.target.value);
  //   console.log(dataMain);
});

// вивід карточок на головній сторінці
function showRandomProducts(productArr) {
  //перевірка чи масив
  if (!Array.isArray(productArr)) {
    console.warn("Не масив!");
    return;
  }
  productArr.forEach((el) => {
    console.log(el);
    const card = `
    <div class="cards">
          <div class="card-sale">
            <img src="${el.availableOptions[0].optionImages[0]}" alt="${el.productName}" />
            <div class="name-product">${el.productName}</div>
            <img src="./img/Group 1 star.png" alt="logo" />
            <div class="price">As low as UAH ${el.availableOptions[0].prices[0].price}</div>
            <button class="btn-add">🛒 ADD TO CARD</button>
          </div>
    `;
    document.querySelector(".cards").insertAdjacentHTML("beforeend", card);
  });
  console.log(productArr);
}
